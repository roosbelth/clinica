<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;

use App\Http\livewire\RolController;
use App\Http\livewire\ServicioController;
use App\Http\livewire\TipoPacienteController;
use App\Http\livewire\EspecialidadController;
use App\Http\Livewire\MedicamentoController;
use App\Http\Livewire\RecetaMedicaController;
use App\Http\Livewire\MedicoController;
use App\Http\Livewire\PacienteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('/admin', function () {
    return view('admin.dashboard');
});*/



Route::middleware(['auth', 'role:1'])->group(function () {
    // User is authentication and has admin role
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});


Route::get('/login', [App\Http\Controllers\LoginController::class, 'show'])->name('login');
Route::post('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('login.perform');

Route::middleware(['middleware' => 'auth'])->group(function() {
    //convocatoria
    Route::group([
        //'namespace' => 'admin',
        'middleware' => 'admin'],
        function () {

            Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
            Route::post('/logout', [App\Http\Controllers\LogoutController::class, 'perform'])->name('logout');

            //Route::get('/administrar/rol', [App\Http\Controllers\HomeController::class, 'index'])->name('admin.rol');
            Route::resource('/roles', RolController::class);
            Route::resource('/servicios', ServicioController::class);
            Route::resource('/tipo-paciente', TipoPacienteController::class);
            Route::resource('/especialidad', EspecialidadController::class);
            Route::resource('/medicamento', MedicamentoController::class);
            Route::resource('/receta-medica', RecetaMedicaController::class);
            Route::resource('/medicos', MedicoController::class);
            Route::resource('/pacientes', PacienteController::class);
        });

    Route::group([
        //'namespace' => 'admin',
        'middleware' => 'empleado'],
        function () {

            //Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
            //Route::post('/logout', [App\Http\Controllers\LogoutController::class, 'perform'])->name('logout');

            Route::get('/administrar/rol', [App\Http\Controllers\HomeController::class, 'index1'])->name('home1');

        });
    });

//Route::middleware(['auth', 'role:1'])->group(function () {
    // User is authentication and has admin role
    /*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::post('/logout', [App\Http\Controllers\LogoutController::class, 'perform'])->name('logout');*/
//});
