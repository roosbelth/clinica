<?php

namespace App\Http\Livewire;

use App\Models\Medicamento;
use Livewire\Component;
use Livewire\WithPagination;

class MedicamentoController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $descripcion, $nombre, $marca, $estado, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';

    protected $rules = [
        'nombre'       => 'required',
        'marca'   => 'required',
        'descripcion'   => 'required',
    ];

    protected $msjError = [
        'nombre.required' => 'El campo nombre es obligatorio.',
        'marca.required' => 'El campo marca es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.'
    ];
    public function render()
    {
        $medicamentos = Medicamento::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.medicamento.view', compact('medicamentos'));
    }
    public function index()
    {
        return view('admin.medicamento.index');
    }
    public function create()
    {
        $this->resetUI();
        $this->emit('show-modal-tmedicamento', 'mostrar modal');
    }
    public function store()
    {

        $this->validate($this->rules, $this->msjError);
        Medicamento::create([
            'nombre' => $this->nombre,
            'marca' => $this->marca,
            'descripcion' => $this->descripcion,
            'estado' => 1,
            'created_at' => now(),
            'updated_at' => null
        ]);

        $this->resetUI();
        $this->emit('tmedicamento-add-close', 'Medicamento Registrado');
        $this->dispatchBrowserEvent(
            'alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']
        );
    }

    public function edit($id)
    {
        $this->selected_id = $id;
        $rol = Medicamento::find($id);
        $this->nombre = $rol->nombre;
        $this->marca = $rol->marca;
        $this->descripcion = $rol->descripcion;
        $this->emit('show-modal-tmedicamento', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Medicamento::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'marca' => $this->marca,
                'descripcion' => $this->descripcion,
                'updated_at' => now()
            ]);

            $this->resetUI();
            $this->emit('tmedicamento-add-close', 'Medicamento Actualizado');
            $this->dispatchBrowserEvent(
                'alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']
            );
        }
    }

    public function deleteForm($id, $accion)
    {
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado()
    {
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }
    public function closeModal()
    {
        $this->resetUI();
        $this->emit('tmedicamento-add-close', 'Medicamento Actualizado');
    }
    public function resetUI()
    {
        $this->nombre = null;
        $this->marca = null;
        $this->descripcion = null;
        $this->selected_id = null;
        $this->accion = null;
        $this->resetValidation();
    }
    public function eliminar()
    {
        //dd($this->accion);
        if ($this->selected_id) {
            if ($this->accion == 'desactivar') {
                $record = Medicamento::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent(
                    'alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']
                );
            } else {
                $record = Medicamento::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent(
                    'alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']
                );
            }
        }
    }
}
