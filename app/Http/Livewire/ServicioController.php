<?php

namespace App\Http\Livewire;

use App\Models\Servicio;
use Livewire\Component;
use Livewire\WithPagination;

class ServicioController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $nombre, $descripcion, $precio, $estado, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'nombre'       => 'required',
        'nombre' => 'required|alpha',
        'precio'       => 'required|numeric|min:0|max:100000',
        'descripcion'   => 'required',

    ];

    protected $msjError=[
        'nombre.required' => 'El campo nombre del servicio es obligatorio.',
        'nombre.alpha' => 'El nombre solo debe contener letras.',
        'precio.required' => 'El campo precio del servicio es obligatorio.',
        'precio.numeric'=>'Ingresar un valor numerico',
        'precio.min'=>'Ingresar un valor mayor ó igual a cero',
        'precio.max'=>'Ingresar un valor menor a 100000',

        'descripcion.required' => 'El campo descripción es obligatorio.'
    ];

    public function render()
    {
        $roles = Servicio::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.servicios.view', compact('roles'));
    }

    public function index()
    {
        return view('admin.servicios.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-servicio', 'mostrar modal');
    }

    public function store(){

        $this->validate($this->rules, $this->msjError);
        Servicio::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('servicio-add-close', 'Servicio Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function resetUI(){
        $this->nombre=null;
        $this->precio=null;
        $this->descripcion=null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->selected_id = $id;
        $rol = Servicio::find($id);
        $this->nombre = $rol->nombre;
        $this->precio = $rol->precio;
        $this->descripcion =$rol->descripcion;
        $this->emit('show-modal-servicio', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Servicio::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'precio' => $this->precio,
                'created_at'=>null,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('servicio-add-close', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('servicio-add-close', 'Rol Actualizado');
    }

    public function eliminar()
    {
        //dd($this->accion);
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = Servicio::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = Servicio::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }
}
