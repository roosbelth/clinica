<?php

namespace App\Http\Livewire;

use App\Models\RecetaMedica;
use Livewire\Component;
use Livewire\WithPagination;

class RecetaMedicaController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $descripcion, $tratamiento, $estado, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'descripcion' => 'required',
        'tratamiento' => 'required'
    ];

    protected $msjError = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'tratamiento.required' => 'El campo tratamiento es obligatorio.'
    ];

    public function render()
    {
        $receta_medicas = RecetaMedica::where('descripcion', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.receta-medica.view',compact('receta_medicas'));
    }
    public function index(){
        return view('admin.receta-medica.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-treceta-medica', 'mostrar modal');
    }

    public function store(){

        $this->validate($this->rules, $this->msjError);
        RecetaMedica::create([
            'descripcion' => $this->descripcion,
            'tratamiento' => $this->tratamiento,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('treceta-medica-add-close', 'Receta medica Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function edit($id){
        $this->selected_id = $id;
        $rol = RecetaMedica::find($id);
        $this->descripcion =$rol->descripcion;
        $this->tratamiento = $rol->tratamiento;
        $this->emit('show-modal-treceta-medica', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = RecetaMedica::find($this->selected_id);
            $record->update([
                'descripcion' => $this->descripcion,
                'tratamiento' => $this->tratamiento,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('treceta-medica-add-close', 'Receta Medica Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('treceta-medica-add-close', 'Receta Medica Actualizado');
    }

    public function resetUI(){
        $this->descripcion=null;
        $this->tratamiento=null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }

    public function eliminar()
    {
        //dd($this->accion);
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = RecetaMedica::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = RecetaMedica::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }
}
