<?php

namespace App\Http\Livewire;

use App\Models\Rol;
use Livewire\Component;
use Livewire\WithPagination;

class RolController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $title = "Xfiiiiiiiiiiiiin";
    public $accion;

    public $members = 'members';
    public $selected_id;
    public $nombreRol;
    public $descripcion;
    public $search;
    public $cantidad=5;
    public $sort='id';
    public $order='desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombreRol'       => 'required',
        'descripcion'   => 'required',
    ];

    protected $msjError=[
        'nombreRol.required' => 'El campo rol es obligatorio.',
        'descripcion.required' => 'El campo dwcripción es obligatorio.'
    ];

    public function render()
    {
        $roles = Rol::where('rol','like','%'.$this->search.'%')
        ->orderBy($this->sort,$this->order)
        ->paginate($this->cantidad);
        return view('admin.roles.listar', compact('roles'));
    }

    public function index()
    {
        return view('admin.roles.index');
    }

    public function store(){

        $this->validate($this->rules, $this->msjError);
        Rol::create([
            'rol' => $this->nombreRol,
            'descripcion' => $this->descripcion,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('rol-add', 'Rol Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function resetUI(){
        $this->nombreRol='';
        $this->descripcion='';
        $this->selected_id=null;
        $this->resetValidation();
    }

    public function edit($id){
        $this->selected_id = $id;
        $rol = Rol::find($id);
        $this->nombreRol = $rol->rol;
        $this->descripcion =$rol->descripcion;
        $this->emit('show-modal', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Rol::find($this->selected_id);
            $record->update([
                'rol' => $this->nombreRol,
                'descripcion' => $this->descripcion,
                'created_at'=>null,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('rol-add', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Registro exitoso!']);
        }
    }

    /*public function alertSuccess()
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'User Created Successfully!']);
    }

    public function alertError()
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'error',  'message' => 'Something is Wrong!']);
    }

    public function alertInfo()
    {
        $this->dispatchBrowserEvent('alert',
            ['type' => 'info',  'message' => 'Going Well!']);
    }*/
}
