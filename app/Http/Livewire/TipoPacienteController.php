<?php

namespace App\Http\Livewire;

use App\Models\TipoPaciente;
use Livewire\Component;
use Livewire\WithPagination;

class TipoPacienteController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $tipo_paciente, $descripcion, $precio, $estado, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'tipo_paciente' => 'required',
        'tipo_paciente' => 'required|alpha',
        'descripcion'   => 'required',
    ];

    protected $msjError=[
        'tipo_paciente.required' => 'El campo tipo paciente es obligatorio.',
        'tipo_paciente.alpha' => 'El tipo paciente solo debe contener letras.',
        'descripcion.required' => 'El campo descripción es obligatorio.'
    ];

    public function render()
    {
        $tipo_pacientes = TipoPaciente::where('tipo_paciente', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.tipo-paciente.view', compact('tipo_pacientes'));
    }

    public function index()
    {
        return view('admin.tipo-paciente.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-tpaciente', 'mostrar modal');
    }

    public function store(){

        $this->validate($this->rules, $this->msjError);
        TipoPaciente::create([
            'tipo_paciente' => $this->tipo_paciente,
            'descripcion' => $this->descripcion,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('tpaciente-add-close', 'Servicio Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function edit($id){
        $this->selected_id = $id;
        $rol = TipoPaciente::find($id);
        $this->tipo_paciente = $rol->tipo_paciente;
        $this->descripcion =$rol->descripcion;
        $this->emit('show-modal-tpaciente', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = TipoPaciente::find($this->selected_id);
            $record->update([
                'tipo_paciente' => $this->tipo_paciente,
                'descripcion' => $this->descripcion,
                'created_at'=>null,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('tpaciente-add-close', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('tpaciente-add-close', 'Rol Actualizado');
    }

    public function resetUI(){
        $this->tipo_paciente=null;
        $this->precio=null;
        $this->descripcion=null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }

    public function eliminar()
    {
        //dd($this->accion);
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = TipoPaciente::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = TipoPaciente::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }
}
