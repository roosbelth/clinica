<?php

namespace App\Http\Livewire;

use App\Models\Especialidad;
use Livewire\Component;
use Livewire\WithPagination;

class EspecialidadController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $nombre, $descripcion, $estado, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'nombre'       => 'required',
        'nombre' => 'required|alpha',
        

        'descripcion'   => 'required',
    ];

    protected $msjError=[
        'nombre.required' => 'El campo tipo nombre especialidad es obligatorio.',
        'nombre.alpha' => 'El nombre solo debe contener letras.',
       
        
        'descripcion.required' => 'El campo descripción es obligatorio.'

    ];

    public function render()
    {
        $especialidades = Especialidad::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.especialidad.view', compact('especialidades'));
    }

    public function index()
    {
        return view('admin.especialidad.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-especialidad', 'mostrar modal');
    }

    public function store(){
        $this->validate($this->rules, $this->msjError);
        Especialidad::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('especialidad-add-close', 'Servicio Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function edit($id){
        $this->selected_id = $id;
        $especidad = Especialidad::find($id);
        $this->nombre = $especidad->nombre;
        $this->descripcion =$especidad->descripcion;
        $this->created_at =$especidad->created_at;
        $this->emit('show-modal-especialidad', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Especialidad::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'created_at'=>null,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('especialidad-add-close', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('especialidad-add-close', 'Rol Actualizado');
    }

    public function eliminar()
    {
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = Especialidad::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = Especialidad::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }

    public function resetUI(){
        $this->nombre=null;
        $this->descripcion=null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }
}
