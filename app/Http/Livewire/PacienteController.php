<?php

namespace App\Http\Livewire;

use App\Models\Especialidad;
use App\Models\Medico;
use App\Models\Paciente;
use App\Models\Persona;
use App\Models\TipoPaciente;
use Livewire\Component;
use Livewire\WithPagination;

class PacienteController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $dni, $nombres, $apellido_materno, $apellido_paterno, $fecha_nacimiento, $celular, $correo, $genero,
        $ocupacion, $tipo_paciente_id, $persona_id, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'dni'               => 'required',
        'nombres'           => 'required',
        'apellido_materno'  => 'required',
        'apellido_paterno'  => 'required',
        'fecha_nacimiento'  => 'required',
        'correo'  => 'required',
        'genero'  => 'required',
        'ocupacion'  => 'required',
        'tipo_paciente_id'  => 'required',
        'celular' => 'required'
    ];

    protected $msjError=[
        'dni.required' => 'El campo DNI es obligatorio.',
        'nombres.required' => 'El campo nombres es obligatorio.',
        'apellido_materno.required' => 'El campo apellido materno es obligatorio.',
        'apellido_paterno.required' => 'El campo apellido paterno es obligatorio.',
        //'celular.required' => 'El campo especialidad es obligatorio.',
        //'codigo_medicos.required' => 'El campo código de medico es obligatorio.'
    ];

    public function render()
    {
        $cbo_tipo_paciente = TipoPaciente::where('estado', 1)->get();

        $pacientes = Paciente::select('pacientes.id', 'pacientes.persona_id', 'pacientes.tipo_paciente_id', 'pacientes.ocupacion', 'pacientes.fecha_registro', 'pacientes.estado',
        'persona.dni','persona.nombres','persona.apellido_paterno','persona.apellido_materno','persona.fecha_nacimiento', 'persona.celular', 'persona.correo', 'persona.genero',
        'tipo_pacientes.tipo_paciente')
            ->join('persona', 'pacientes.persona_id', '=', 'persona.id')
            ->join('tipo_pacientes', 'pacientes.tipo_paciente_id', '=', 'tipo_pacientes.id')
            ->where('persona.nombres', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        //dd($data);
        return view('admin.pacientes.view', compact('pacientes', 'cbo_tipo_paciente'));
    }

    public function index()
    {
        return view('admin.pacientes.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-paciente', 'mostrar modal');
    }

    public function store(){
        $this->validate($this->rules, $this->msjError);
        $persona_insertado = Persona::create([
            'dni' => $this->dni,
            'nombres' => $this->nombres,
            'apellido_paterno' => $this->apellido_paterno,
            'apellido_materno' => $this->apellido_materno,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'celular' => $this->celular,
            'correo' => $this->correo,
            'genero' => $this->genero,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->persona_id = $persona_insertado->id;

        $persona_insertado = Paciente::create([
            'persona_id' => $this->persona_id,
            'tipo_paciente_id' => $this->tipo_paciente_id,
            'ocupacion' => $this->ocupacion,
            'fecha_registro' => now(),
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);



        $this->resetUI();
        $this->emit('paciente-add-close', 'Servicio Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function edit($id){
        $this->selected_id = $id;
        $paciente = Paciente::find($id);
        $this->persona_id = $paciente->persona_id;
        $this->tipo_paciente_id = $paciente->tipo_paciente_id;
        $this->ocupacion = $paciente->ocupacion;

        $persona = Persona::find($this->persona_id);
        $this->dni = $persona->dni;
        $this->nombres = $persona->nombres;
        $this->apellido_paterno = $persona->apellido_paterno;
        $this->apellido_materno = $persona->apellido_materno;
        $this->fecha_nacimiento = $persona->fecha_nacimiento;
        $this->celular =$persona->celular;
        $this->correo =$persona->correo;
        $this->genero =$persona->genero;
        $this->emit('show-modal-paciente', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Paciente::find($this->selected_id);
            $record->update([
                'persona_id' => $this->persona_id,
                'tipo_paciente_id' => $this->tipo_paciente_id,
                'ocupacion' => $this->ocupacion,
                'updated_at'=>now()
            ]);

            $record = Persona::find($this->persona_id);
            $record->update([
                'dni' => $this->dni,
                'nombres' => $this->nombres,
                'apellido_paterno' => $this->apellido_paterno,
                'apellido_materno' => $this->apellido_materno,
                'fecha_nacimiento' => $this->fecha_nacimiento,
                'celular' => $this->celular,
                'correo' => $this->correo,
                'genero' => $this->genero,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('paciente-add-close', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('paciente-add-close', 'Rol Actualizado');
    }

    public function eliminar()
    {
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = Paciente::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = Paciente::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }

    public function closeModalShow(){
        $this->resetUI();
        $this->emit('pacienteshow-add-close', 'Rol Actualizado');
    }


    public function show($id){
        $this->selected_id = $id;
        $paciente = Paciente::find($id);
        $this->persona_id = $paciente->persona_id;
        $this->tipo_paciente_id = $paciente->tipo_paciente_id;
        $this->ocupacion = $paciente->ocupacion;

        $persona = Persona::find($this->persona_id);
        $this->dni = $persona->dni;
        $this->nombres = $persona->nombres;
        $this->apellido_paterno = $persona->apellido_paterno;
        $this->apellido_materno = $persona->apellido_materno;
        $this->fecha_nacimiento = $persona->fecha_nacimiento;
        $this->celular =$persona->celular;
        $this->correo =$persona->correo;
        $this->genero =$persona->genero;
        $this->emit('show-modal-pacienteshow', 'mostrar modal');
    }

    public function resetUI(){
        $this->dni=null;
        $this->nombres=null;
        $this->apellido_materno=null;
        $this->apellido_paterno=null;
        $this->fecha_nacimiento=null;
        $this->celular=null;
        $this->correo=null;
        $this->genero=null;
        $this->persona_id=null;
        $this->tipo_paciente_id=null;
        $this->ocupacion = null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }
}
