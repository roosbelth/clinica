<?php

namespace App\Http\Livewire;

use App\Models\Especialidad;
use App\Models\Medico;
use App\Models\Paciente;
use Livewire\Component;
use Livewire\WithPagination;

class MedicoController extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['render', 'destroy'];

    public $selected_id;
    public $accion;
    public $dni, $nombres, $especialidad_id, $apellido_materno, $apellido_paterno, $codigo_medicos, $created_at, $updated_at;
    public $search;
    public $cantidad = 10;
    public $sort = 'id';
    public $order = 'desc';


    protected $rules = [
        'dni'               => 'required',
        'nombres'           => 'required',
        'apellido_materno'  => 'required',
        'apellido_paterno'  => 'required',
        'especialidad_id'  => 'required',
        'codigo_medicos'  => 'required',
    ];

    protected $msjError=[
        'dni.required' => 'El campo DNI es obligatorio.',
        'nombres.required' => 'El campo nombres es obligatorio.',
        'apellido_materno.required' => 'El campo apellido materno es obligatorio.',
        'apellido_paterno.required' => 'El campo apellido paterno es obligatorio.',
        'especialidad_id.required' => 'El campo especialidad es obligatorio.',
        'codigo_medicos.required' => 'El campo código de medico es obligatorio.'
    ];

    public function render()
    {

        //$escritores = Paciente::with('personas')->get();
        //dd($escritores);
        $cbo_especialidad = Especialidad::where('estado', 1)->get();
        $medicos = Medico::where('nombres', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->order)
            ->paginate($this->cantidad);
        return view('admin.medicos.view', compact('medicos', 'cbo_especialidad'));
    }

    public function index()
    {
        return view('admin.medicos.index');
    }

    public function create(){
        $this->resetUI();
        $this->emit('show-modal-medico', 'mostrar modal');
    }

    public function store(){
        $this->validate($this->rules, $this->msjError);
        Medico::create([
            'dni' => $this->dni,
            'nombres' => $this->nombres,
            'apellido_paterno' => $this->apellido_paterno,
            'apellido_materno' => $this->apellido_materno,
            'codigo_medicos' => $this->codigo_medicos,
            'especialidad_id' => $this->especialidad_id,
            'estado' => 1,
            'created_at'=>now(),
            'updated_at'=>null
        ]);

        $this->resetUI();
        $this->emit('medico-add-close', 'Servicio Registrado');
        $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Registro exitoso!']);

    }

    public function edit($id){
        $this->selected_id = $id;
        $medico = Medico::find($id);
        $this->dni = $medico->dni;
        $this->nombres = $medico->nombres;
        $this->apellido_paterno = $medico->apellido_paterno;
        $this->apellido_materno = $medico->apellido_materno;
        $this->especialidad_id = $medico->especialidad_id;
        $this->codigo_medicos =$medico->codigo_medicos;
        $this->created_at =$medico->created_at;
        $this->emit('show-modal-medico', 'mostrar modal');
    }

    public function update()
    {
        $this->validate($this->rules, $this->msjError);

        if ($this->selected_id) {
            $record = Medico::find($this->selected_id);
            $record->update([
                'dni' => $this->dni,
                'nombres' => $this->nombres,
                'apellido_paterno' => $this->apellido_paterno,
                'apellido_materno' => $this->apellido_materno,
                'especialidad_id' => $this->especialidad_id,
                'codigo_medicos' => $this->codigo_medicos,
                //'created_at'=>null,
                'updated_at'=>now()
            ]);

            $this->resetUI();
            $this->emit('medico-add-close', 'Rol Actualizado');
            $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Actualización exitoso!']);
        }
    }

    public function deleteForm($id, $accion){
        $this->selected_id = $id;
        $this->accion = $accion;
        $this->emit('info-alert-modal', 'mostrar modal');
    }

    public function cancelarEliminado(){
        $this->selected_id = null;
        $this->emit('info-alert-modal-close', 'mostrar modal');
    }

    public function closeModal(){
        $this->resetUI();
        $this->emit('medico-add-close', 'Rol Actualizado');
    }

    public function eliminar()
    {
        if ($this->selected_id) {
            if ($this->accion=='desactivar'){
                $record = Medico::find($this->selected_id);
                $record->update([
                    'estado' => 0
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Eliminado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Eliminado!']);
            } else{
                $record = Medico::find($this->selected_id);
                $record->update([
                    'estado' => 1
                ]);

                $this->resetUI();
                $this->emit('info-alert-modal-close', 'Registro Activado');
                $this->dispatchBrowserEvent('alert',
                    ['type' => 'success',  'message' => 'Registro Activado!']);
            }

        }
    }

    public function resetUI(){
        $this->dni=null;
        $this->nombres=null;
        $this->apellido_materno=null;
        $this->apellido_paterno=null;
        $this->codigo_medicos=null;
        $this->especialidad_id=null;
        $this->selected_id=null;
        $this->accion=null;
        $this->resetValidation();
    }
}
