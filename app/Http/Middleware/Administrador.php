<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Administrador
{
    public function handle($request, Closure $next)
    {
        if( Auth::check() )
        {
            //dd(Auth::user()->isAdmin());
            // if user is not admin take him to his dashboard
            if ( Auth::user()->isUser() ) {
                return redirect(route('home1'));
            }

            // allow admin to proceed with request
            else if ( Auth::user()->isAdmin() ) {
                return $next($request);
            }
        }

        abort(404);

        //return redirect()->route('home');
    }
}
