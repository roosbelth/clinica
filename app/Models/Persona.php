<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    protected $table = "persona";
    protected $fillable = ['id', 'dni', 'nombres', 'apellido_paterno', 'apellido_materno', 'fecha_nacimiento',
        'celular', 'correo', 'genero','estado', 'created_at', 'updated_at'];


    public function pacientes(){
        return $this->hasMany('App\Models\Paciente');
    }
}
