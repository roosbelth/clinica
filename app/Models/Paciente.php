<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'persona_id', 'tipo_paciente_id', 'ocupacion', 'fecha_registro', 'estado', 'created_at', 'updated_at'];

    public function persona()
    {
        return $this->belongsTo('App\models\Persona');
    }


    public function tipoPaciente()
    {
        return $this->belongsTo('App\models\TipoPaciente');
    }
}
