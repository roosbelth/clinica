<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'nombre', 'descripcion', 'estado', 'created_at', 'updated_at'];

    public function medicos(){
        return $this->hasMany('App\Models\Medico');
    }
}
