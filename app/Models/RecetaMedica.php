<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecetaMedica extends Model
{
    use HasFactory;
    protected $fillable= ['id',  'descripcion','tratamiento', 'estado', 'created_at', 'updated_at'];
}
