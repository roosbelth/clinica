<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoPaciente extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'tipo_paciente', 'descripcion', 'estado', 'created_at', 'updated_at'];

    public function pacientes(){
        return $this->hasMany('App\Models\Paciente');
    }
}
