<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    use HasFactory;

    protected $fillable = ['id','dni', 'nombres', 'apellido_paterno', 'apellido_materno', 'codigo_medicos', 'especialidad_id', 'estado', 'created_at', 'updated_at'];

    public function especialidad()
    {
        return $this->belongsTo('App\models\Especialidad');
    }
}
