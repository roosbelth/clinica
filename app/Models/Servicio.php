<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'nombre', 'precio', 'descripcion', 'estado', 'created_at', 'updated_at'];
}
