<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('persona_id');
            $table->unsignedBigInteger('tipo_paciente_id');
            $table->string('ocupacion', 150);
            $table->date('fecha_registro')->default(now());
            $table->boolean('estado')->default(1);

            $table->foreign('persona_id')->references('id')->on('persona');
            $table->foreign('tipo_paciente_id')->references('id')->on('tipo_pacientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
};
