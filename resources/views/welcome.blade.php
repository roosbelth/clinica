<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('./images/favicon.ico')}}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('./css/bootstrap.min.css')}}" type="text/css">

    <!--Material Icon -->
    <link rel="stylesheet" href="{{asset('./css/materialdesignicons.min.css')}}" type="text/css">

    <!-- Custom  sCss -->
    <link rel="stylesheet" href="{{asset('./css/style.css')}}" type="text/css">
</head>

<body>

    <!--Navbar Start-->
    <nav class="navbar navbar-expand-md fixed-top navbar-custom sticky sticky-dark">
        <div class="container-fluid">

            <!-- LOGO -->
            <a class="logo text-uppercase col-9" href="/">
                <img src="{{asset('./images/logo-light.png')}}" alt="" class="logo-light" height="21" />
                <img src="{{asset('./images/logo-dark.png')}}" alt="" class="logo-dark" height="21" />
            </a>

            @auth

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mx-auto navbar-center" id="mySidenav">

                    @if(Auth::user()->rol_id == 1)
                    <li class="nav-item">
                        <a href="{{route('home')}}" class="nav-link">INICIO</a>
                    </li>
                    @elseif(Auth::user()->rol_id == 2)
                    <li class="nav-item">
                        <a href="{{route('home1')}}" class="nav-link">INICIO</a>
                    </li>
                    @endif


                    <li class="nav-item">
                        <a href="#pricing" class="nav-link">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a href="#contact" class="nav-link">Contact</a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>

                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </ul>
            </div>
            @else
            <ul class="navbar-nav mx-auto navbar-right" id="mySidenav">
                <li class="nav-item float-right">
                    <a href="{{ route('login') }}" class="nav-link float-right">ACCEDER</a>
                </li>
                {{--<button class="btn btn-info navbar-btn">Try for Free</button>
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
                    data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <i class="mdi mdi-menu"></i>
                </button>--}}
            </ul>
            @endauth

        </div>
    </nav>
    <!-- Navbar End -->

    {{--<div
        class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
        @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth

            @if(Auth::user()->rol_id == 1)
            <a href="{{ route('home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @elseif(Auth::user()->rol_id == 2)
            <a href="{{ route('admin.rol') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @endif
            @else
            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}"
                class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
            @endif
            @endauth
        </div>
        @endif
    </div>--}}

    <!-- home start -->
    <section class="bg-home bg-gradient" id="home">
        <div class="home-center">
            <div class="home-desc-center">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="home-title mo-mb-20">
                                <h1 class="mb-4 text-white">Ubold is a fully featured premium admin template</h1>
                                <p class="text-white-50 home-desc mb-5">Ubold is a fully featured premium admin template
                                    built on top of awesome Bootstrap 4.4.1, modern web technology HTML5, CSS3 and
                                    jQuery. It has many ready to use hand crafted components. </p>
                                <div class="subscribe">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="mb-2">
                                                    <input type="text" class="form-control"
                                                        placeholder="Enter your e-mail address">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-primary">Subscribe Us</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-7">
                            <div class="home-img position-relative">
                                <img src="{{asset('./images/home-img.png')}}" alt="" class="home-first-img">
                                <img src="{{asset('./images/home-img.png')}}" alt=""
                                    class="home-second-img mx-auto d-block">
                                <img src="{{asset('./images/home-img.png')}}" alt="" class="home-third-img">
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container-fluid -->
            </div>
        </div>
    </section>
    <!-- home end -->

    <!-- pricing start -->
    <section class="section pb-0 bg-gradient" id="pricing">
        <div class="bg-shape">
            <img src="images/bg-shape.png" alt="" class="img-fluid mx-auto d-block">
        </div>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4">
                        <h3 class="text-white">Choose Our Pricing Plans</h3>
                        <p class="text-white-50">The clean and well commented code allows easy customization of the
                            theme.It's designed for describing your app, agency or business.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="row align-items-center">
                        <div class="col-lg-4">
                            <div class="pricing-plan bg-white p-4 mt-4">
                                <div class="pricing-header text-center">
                                    <h5 class="plan-title text-uppercase mb-4">Single Application</h5>
                                    <h1><sup><small>$</small></sup>49</h1>
                                    <div class="plan-duration text-muted">Per License</div>
                                </div>
                                <ul class="list-unstyled pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>1</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn btn-primary">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-4">
                            <div class="pricing-plan active p-4 mt-4">
                                <span class="lable">Popular</span>
                                <div class="pricing-header text-white text-center">
                                    <h5 class="plan-title text-white text-uppercase mb-4">Multiple Application</h5>
                                    <h1 class=" text-white"><sup><small>$</small></sup>149</h1>
                                    <div class="plan-duration">Per License</div>
                                </div>
                                <ul class="list-unstyled text-white pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>5</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn shadow btn-light">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-lg-4">
                            <div class="pricing-plan bg-white p-4 mt-4">
                                <div class="pricing-header text-center">
                                    <h5 class="plan-title text-uppercase mb-4">Extended</h5>
                                    <h1><sup><small>$</small></sup>999</h1>
                                    <div class="plan-duration text-muted">Per License</div>
                                </div>
                                <ul class="list-unstyled pricing-list mt-4">
                                    <li>
                                        <i class="mdi mdi-album"></i>
                                        <p>Number of end products <b>1</b></p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-lifebuoy"></i>
                                        <p>Customer support
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-update"></i>
                                        <p>Free Updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-earth"></i>
                                        <p> 1 Domain
                                        <p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-history"></i>
                                        <p>Monthly updates</p>
                                    </li>
                                    <li>
                                        <i class="mdi mdi-alarm-check"></i>
                                        <p>24x7 Support</p>
                                    </li>
                                </ul>
                                <div class="text-center mt-5">
                                    <a href="#" class="btn btn-primary">Purchase Now</a>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                </div> <!-- end col-xl-10 -->

            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- pricing end -->

    <!-- testimonial start -->
    <section class="section bg-light" id="clients">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mb-4">
                        <h3>What our Users Says</h3>
                        <p class="text-muted">The clean and well commented code allows easy customization of the
                            theme.It's designed for describing your app, agency or business.</p>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Excellent support for a tricky issue related to our
                                customization of the template. Author kept us updated as he made progress on the issue
                                and emailed us a patch when he was done. "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="{{asset('./images/testi/img-2.png')}}" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">Michael Morrell</h5>
                                <p class="text-muted m-0"><small>- Ubold User</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Flexible, Everything is in, Suuuuuper light, even for the code
                                is much easier to cut and make it a theme for a productive app. "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="{{asset('./images/testi/img-1.png')}}" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">John Seidel</h5>
                                <p class="text-muted m-0"><small>- Ubold User</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
                <div class="col-lg-4">
                    <div class="testi-box mt-4">
                        <div class="testi-desc bg-white p-4">
                            <p class="text-muted mb-0">" Not only the code, design and support are awesome, but they
                                also update it constantly the template with new content, new plugins. I will buy surely
                                another coderthemes template! "</p>
                        </div>
                        <div class="p-4">
                            <div class="testi-img float-left mr-2">
                                <img src="{{asset('./images/testi/img-3.png')}}" alt="" class="rounded-circle">
                            </div>
                            <div>
                                <h5 class="mb-0">Robert Taylor</h5>
                                <p class="text-muted m-0"><small>- Ubold User</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- testimonial end -->

    <!-- cta start -->
    <section class="section-sm bg-light">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-9">
                    <h3 class="mb-0 mo-mb-20">We also customize the theme as per your needs</h3>
                </div>
                <div class="col-md-3">
                    <div class="text-md-right">
                        <a href="#" class="btn btn-outline-dark btn-rounded"><i class="mdi mdi-email-outline mr-1"></i>
                            Contact Us</a>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </section>
    <!-- cta end -->

    <!-- footer start -->
    <footer class="bg-dark footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left pull-none">
                        <p class="text-white-50">2015 - 2020 © Ubold. Design by <a href="https://coderthemes.com/"
                                target="_blank" class="text-white-50">Coderthemes</a> </p>
                    </div>
                    <div class="float-right pull-none">
                        <ul class="list-inline social-links">
                            <li class="list-inline-item text-white-50">
                                Social :
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="mdi mdi-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="mdi mdi-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="mdi mdi-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="mdi mdi-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </footer>
    <!-- footer end -->

    <!-- Back to top -->
    <a href="#" class="back-to-top" id="back-to-top"> <i class="mdi mdi-chevron-up"> </i> </a>



    <!-- javascript -->
    <script src="{{asset('./js/jquery.min.js')}}"></script>
    <script src="{{asset('./js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('./js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('./js/scrollspy.min.js')}}"></script>

    <!-- custom js -->
    <script src="{{asset('./js/app.js')}}"></script>
</body>

</html>
