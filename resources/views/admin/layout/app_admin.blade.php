
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Citas | Clinica NOMBRE DE LA CLINICA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="desarrollado por roosbelth eufrain alva flores" name="description" />
    <meta content="pentatools sac" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('./assets/images/favicon.ico')}}">
    <!-- Responsive Table css -->
    <link href="{{asset('./assets/libs/admin-resources/rwd-table/rwd-table.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{asset('./assets/css/bootstrap-modern.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{asset('./assets/css/app-modern.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <link href="{{asset('./assets/css/bootstrap-modern-dark.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
    <link href="{{asset('./assets/css/app-modern-dark.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

    <!-- icons -->
    <link href="{{asset('./assets/css/icons.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('./assets/css/toastr.min.css')}}" rel="stylesheet" type="text/css">

    @livewireStyles
</head>

<body data-layout-mode="detached" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "dark", "size": "default", "showuser": true}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
    @include('admin.layout.topbar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    @include('admin.layout.sidebar')
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content" style="height: 100% !important;">

            @yield('content')

            <!-- Footer Start -->
            @include('admin.layout.footer')
            <!-- end Footer -->
        </div> <!-- content -->



    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Vendor js -->
<script src="{{asset('./assets/js/vendor.min.js')}}"></script>

<!-- Responsive Table js -->
<script src="{{asset('./assets/libs/admin-resources/rwd-table/rwd-table.min.js')}}"></script>

<!-- Init js -->
<script src="{{asset('./assets/js/pages/responsive-table.init.js')}}"></script>


<script src="{{asset('./assets/js/toastr.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('./assets/js/app.min.js')}}"></script>
@livewireScripts

<script>
    window.addEventListener('alert', event => {
        toastr[event.detail.type](event.detail.message,
            event.detail.title ?? ''), toastr.options = {
            "closeButton": true,
            "progressBar": true,
        }
    });
</script>
</body>
</html>
