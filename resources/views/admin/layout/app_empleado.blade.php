
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Citas | Clinica NOMBRE DE LA CLINICA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="desarrollado por roosbelth eufrain alva flores" name="description" />
    <meta content="pentatools sac" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('./assets/images/favicon.ico')}}">

    <link href="./assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
    <link href="./assets/libs/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="{{asset('./assets/css/bootstrap-modern.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{asset('./assets/css/app-modern.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <link href="{{asset('./assets/css/bootstrap-modern-dark.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
    <link href="{{asset('./assets/css/app-modern-dark.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <!-- icons -->
    <link href="{{asset('./assets/css/icons.css')}}" rel="stylesheet" type="text/css" />

    @livewireStyles
</head>

<body data-layout-mode="detached" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "dark", "size": "default", "showuser": true}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
    @include('admin.layout.topbar')
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    @include('admin.layout.sidebar_empleado')
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            @yield('content')

        </div> <!-- content -->

        <!-- Footer Start -->
        @include('admin.layout.footer')
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->
<script src="./assets/libs/selectize/js/standalone/selectize.min.js"></script>
<script src="./assets/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Vendor js -->
<script src="{{asset('./assets/js/vendor.min.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<!-- App js -->
<script src="{{asset('./assets/js/app.min.js')}}"></script>
@livewireScripts

<script>
    window.addEventListener('alert', event => {
        toastr[event.detail.type](event.detail.message,
            event.detail.title ?? ''), toastr.options = {
            "closeButton": true,
            "progressBar": true,
        }
    });
</script>
</body>
</html>
