<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                        <li class="breadcrumb-item active">Starter</li>
                    </ol>
                </div>
                <h4 class="page-title">Pacientes</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-6 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="inputPassword2" class="sr-only">Search</label>
                            <input wire:model="search" type="search" class="form-control" id="inputPassword2" placeholder="Search...">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="text-lg-right mt-3 mt-lg-0">
                            {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#info-alert-modal">Info Alert</button>--}}
                            <button wire:click="create()" type="button" class="btn btn-success waves-effect waves-light mr-1" ><i class="mdi mdi-plus-circle mr-1"></i> AGREGAR</button>
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="responsive-table-plugin">
                    {{--<div class="table-rep-plugin">--}}
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>N°</th>
                                <th>DNI</th>
                                <th>NOMBRES</th>
                                <th>APE. PATERNO</th>
                                <th>APE. MATERNO</th>
                                {{--<th>FECHA NACIMIENTO</th>--}}
                                <th>CELULAR</th>
                                {{--<th>CORREO</th>--}}
                                {{--<th>GENERO</th>--}}
                                {{--<th>OCUPACIÓN</th>--}}
                                {{--<th>FECHA REGISTRO</th>--}}
                                <th>T. PACIENTE</th>
                                <th>ESTADO</th>
                                <th>OPCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pacientes as $medico)
                                <tr>
                                    <td>{{(($pacientes->currentpage()-1)*$cantidad) + $loop->iteration }}</td>
                                    <th class="text-uppercase"><span class="co-name">{{$medico->dni}}</span></th>
                                    <td>{{$medico->nombres}}</td>
                                    <td>{{$medico->apellido_paterno}}</td>
                                    <td>{{$medico->apellido_materno}}</td>
                                    {{--<td> {{ \Carbon\Carbon::parse(strtotime($medico->fecha_nacimiento))->format('d/m/Y') }} </td>--}}
                                    <td>{{$medico->celular}}</td>
                                    {{--<td>{{$medico->correo}}</td>--}}
                                    {{--<td>{{$medico->genero}}</td>--}}
                                    {{--<td>{{$medico->ocupacion}}</td>--}}
                                    {{--<td> {{ \Carbon\Carbon::parse(strtotime($medico->fecha_registro))->format('d/m/Y') }} </td>--}}
                                    <td>{{$medico->tipo_paciente}}</td>
                                    <td>
                                        @if($medico->estado==1)
                                        <span class="badge bg-soft-success text-success">ACTIVADO</span>
                                        @else
                                            <span class="badge bg-soft-danger text-danger">DESACTIVADO</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#info-alert-modal{{ $loop->iteration }}">Info Alert</button>--}}
                                        <button wire:click="edit({{$medico->id}})" type="button" class="btn btn-blue btn-sm waves-effect waves-light"><i class="fas fa-pen"></i></button>
                                        <button wire:click="show({{$medico->id}})" type="button" class="btn btn-info btn-sm waves-effect waves-light"><i class="far fa-eye"></i></button>
                                        @if($medico->estado==1)
                                        <button wire:click="deleteForm({{$medico->id}}, 'desactivar')" type="button" class="btn btn-danger btn-sm" ><i class="fas fa-trash-alt"></i></button>
                                        @else
                                            <button wire:click="deleteForm({{$medico->id}}, 'activar')" type="button" class="btn btn-success btn-sm" ><i class="fas fa-check-circle"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @include('admin.common.delete-dialog')
                                @include('admin.pacientes.show')
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{--</div>--}}
                    <nav class="p-1">
                        <ul class="justify-content-start">
                            {{$pacientes->firstItem()}}
                            a {{$pacientes->lastItem()}} de {{$pacientes->total()}} entradas
                        </ul>
                        <ul class="pagination justify-content-end">

                            {{$pacientes->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    @include('admin.pacientes.form')


</div>

<script>
    document.addEventListener('DOMContentLoaded', function (){
        window.livewire.on('paciente-add-close', msg=>{
            $('#con-close-modal-paciente').modal('hide')
        });

        window.livewire.on('show-modal-paciente', msg=>{
            $('#con-close-modal-paciente').modal('show')
        });

        window.livewire.on('info-alert-modal', msg=>{
            $('#info-alert-modal').modal('show')
        });

        window.livewire.on('info-alert-modal-close', msg=>{
            $('#info-alert-modal').modal('hide')
        });



        window.livewire.on('pacienteshow-add-close', msg=>{
            $('#con-close-modal-pacienteshow').modal('hide')
        });

        window.livewire.on('show-modal-pacienteshow', msg=>{
            $('#con-close-modal-pacienteshow').modal('show')
        });
    });
</script>
