<div wire:ignore.self id="con-close-modal-pacienteshow" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title">PACIENTE | DETALLES</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-3" class="control-label">DNI</label>
                            <input wire:model.lazy="dni" type="text" class="form-control" id="field-3" placeholder="DNI" disabled>
                            @error('dni')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Nombres</label>
                            <input wire:model.lazy="nombres" type="text" class="form-control" id="field-3" placeholder="Nombres" disabled>
                            @error('nombres')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Apellido Paterno</label>
                            <input wire:model.lazy="apellido_paterno" type="text" class="form-control" id="field-3" placeholder="Apellido Paterno" disabled>
                            @error('apellido_paterno')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Apellido Materno</label>
                            <input wire:model.lazy="apellido_materno" type="text" class="form-control" id="field-3" placeholder="Apellido Materno" disabled>
                            @error('apellido_materno')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-date">Fecha Nacimiento</label>
                            <input wire:model.lazy="fecha_nacimiento" class="form-control" id="example-date" type="date" name="date" disabled>
                            @error('fecha_nacimiento')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Celular</label>
                            <input wire:model.lazy="celular" type="text" class="form-control" id="field-3" placeholder="Código medico" disabled>
                            @error('celular')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Correo</label>
                            <input wire:model.lazy="correo" type="email" class="form-control" id="field-3" placeholder="Especialidad" disabled>
                            @error('correo')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <label for="select-code-language">Genero</label>
                            <select wire:model.lazy="genero" class="custom-select form-control" disabled>
                                <option selected>Selecione Genero</option>
                                <option value="F">Femenino</option>
                                <option value="M">Masculino</option>
                            </select>
                            @error('genero')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Ocupación</label>
                            <input wire:model.lazy="ocupacion" type="text" class="form-control" id="field-3" placeholder="Código medico" disabled>
                            @error('ocupacion')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <label for="select-code-language">Tipo Paciente</label>
                            <select wire:model.lazy="tipo_paciente_id" class="custom-select form-control" disabled>
                                <option selected>Seleccione Tipo Paciente</option>
                                @foreach($cbo_tipo_paciente as $item)
                                    <option value="{{$item->id}}">{{$item->tipo_paciente}}</option>
                                @endforeach
                            </select>
                            @error('tipo_paciente_id')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button wire:click="closeModalShow()" type="button" class="btn btn-secondary waves-effect">CERRAR</button>
            {{--    @if(!$selected_id)
                    <button wire:click.prevent="store()" type="button" class="btn btn-primary waves-effect waves-light">REGISTRAR</button>
                @else
                    <button wire:click.prevent="update()" type="button" class="btn btn-primary waves-effect waves-light">ACTUALIZAR</button>
                @endif--}}
            </div>
        </div>
    </div>
</div>
