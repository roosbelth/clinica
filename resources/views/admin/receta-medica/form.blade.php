<div wire:ignore.self id="con-close-modal-treceta-medica" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                @if(!$selected_id)
                    {{--<h4 class="modal-title">{{$title}}</h4>--}}
                    <h4 class="modal-title">RECETA MEDICA | REGISTRAR</h4>
                @else
                    <h4 class="modal-title">RECETA MEDICA | EDITAR</h4>
                @endif
                {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Tratamiento</label>
                            <input wire:model.lazy="tratamiento" type="text" class="form-control" id="field-3" placeholder="Tratamiento">
                            @error('tratamiento')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <label for="field-7" class="control-label">Descripción</label>
                            <textarea wire:model.lazy="descripcion" class="form-control" id="field-7" placeholder="Descripción"></textarea>
                            @error('descripcion')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button wire:click="closeModal()" type="button" class="btn btn-secondary waves-effect">CANCELAR</button>
                @if(!$selected_id)
                    <button wire:click.prevent="store()" type="button" class="btn btn-primary waves-effect waves-light">REGISTRAR</button>
                @else
                    <button wire:click.prevent="update()" type="button" class="btn btn-primary waves-effect waves-light">ACTUALIZAR</button>
                @endif
            </div>
        </div>
    </div>
</div>
