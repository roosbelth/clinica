<div wire:ignore.self id="con-close-modal-medico" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                @if(!$selected_id)
                    {{--<h4 class="modal-title">{{$title}}</h4>--}}
                    <h4 class="modal-title">MEDICOS | REGISTRAR</h4>
                @else
                    <h4 class="modal-title">MEDICOS | EDITAR</h4>
                @endif
                {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-3" class="control-label">DNI</label>
                            <input wire:model.lazy="dni" type="text" class="form-control" id="field-3" placeholder="DNI">
                            @error('dni')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Nombres</label>
                            <input wire:model.lazy="nombres" type="text" class="form-control" id="field-3" placeholder="Nombres">
                            @error('nombres')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Apellido Paterno</label>
                            <input wire:model.lazy="apellido_paterno" type="text" class="form-control" id="field-3" placeholder="Apellido Paterno">
                            @error('apellido_paterno')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Apellido Materno</label>
                            <input wire:model.lazy="apellido_materno" type="text" class="form-control" id="field-3" placeholder="Apellido Materno">
                            @error('apellido_materno')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Código medico</label>
                            <input wire:model.lazy="codigo_medicos" type="text" class="form-control" id="field-3" placeholder="Código medico">
                            @error('codigo_medicos')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    {{--<div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Especialidad</label>
                            <input wire:model.lazy="especialidad_id" type="text" class="form-control" id="field-3" placeholder="Especialidad">
                            @error('especialidad_id')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>--}}
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <label for="select-code-language">Dropdown Header</label>
                            <select wire:model.lazy="especialidad_id" class="custom-select form-control" >
                                <option selected>Open this select menu</option>
                                @foreach($cbo_especialidad as $item)
                                <option value="{{$item->id}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                            @error('especialidad_id')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button wire:click="closeModal()" type="button" class="btn btn-secondary waves-effect">CANCELAR</button>
                @if(!$selected_id)
                    <button wire:click.prevent="store()" type="button" class="btn btn-primary waves-effect waves-light">REGISTRAR</button>
                @else
                    <button wire:click.prevent="update()" type="button" class="btn btn-primary waves-effect waves-light">ACTUALIZAR</button>
                @endif
            </div>
        </div>
    </div>
</div>
