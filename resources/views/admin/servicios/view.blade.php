<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                        <li class="breadcrumb-item active">Starter</li>
                    </ol>
                </div>
                <h4 class="page-title">Servicios</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-6 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="inputPassword2" class="sr-only">Search</label>
                            <input wire:model="search" type="search" class="form-control" id="inputPassword2" placeholder="Search...">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="text-lg-right mt-3 mt-lg-0">
                            {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#info-alert-modal">Info Alert</button>--}}
                            <button wire:click="create()" type="button" class="btn btn-success waves-effect waves-light mr-1" ><i class="mdi mdi-plus-circle mr-1"></i> AGREGAR</button>
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="responsive-table-plugin">
                    {{--<div class="table-rep-plugin">--}}
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>N°</th>
                                <th>SERVICIO</th>
                                <th>PRECIO</th>
                                <th>DESCRIPCIÓN </th>
                                <th>FECHA CREACIÓN</th>
                                <th>FECHA ACTUALIZACIÓN</th>
                                <th>OPCIONES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $rol)
                                <tr>
                                    <td>{{(($roles->currentpage()-1)*$cantidad) + $loop->iteration }}</td>
                                    <th class="text-uppercase"><span class="co-name">{{$rol->nombre}}</span></th>
                                    <th><span class="co-name">{{$rol->precio}}</span></th>
                                    <td>{{$rol->descripcion}}</td>
                                    <td>{{$rol->created_at}}</td>
                                    <td>{{$rol->updated_at}}</td>
                                    <td>
                                        {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#info-alert-modal{{ $loop->iteration }}">Info Alert</button>--}}
                                        <button wire:click="edit({{$rol->id}})" type="button" class="btn btn-blue btn-sm waves-effect waves-light"><i class="fas fa-pen"></i></button>
                                        @if($rol->estado==1)
                                        <button wire:click="deleteForm({{$rol->id}}, 'desactivar')" type="button" class="btn btn-danger btn-sm" ><i class="fas fa-trash-alt"></i></button>
                                        @else
                                            <button wire:click="deleteForm({{$rol->id}}, 'activar')" type="button" class="btn btn-success btn-sm" ><i class="fas fa-check-circle"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @include('admin.common.delete-dialog')
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{--</div>--}}
                    <nav class="p-1">
                        <ul class="justify-content-start">
                            {{$roles->firstItem()}}
                            a {{$roles->lastItem()}} de {{$roles->total()}} entradas
                        </ul>
                        <ul class="pagination justify-content-end">

                            {{$roles->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    @include('admin.servicios.form')


</div>

<script>
    document.addEventListener('DOMContentLoaded', function (){
        window.livewire.on('servicio-add-close', msg=>{
            $('#con-close-modal-servicio').modal('hide')
        });

        window.livewire.on('show-modal-servicio', msg=>{
            $('#con-close-modal-servicio').modal('show')
        });

        window.livewire.on('info-alert-modal', msg=>{
            $('#info-alert-modal').modal('show')
        });

        window.livewire.on('info-alert-modal-close', msg=>{
            $('#info-alert-modal').modal('hide')
        });
    });
</script>
