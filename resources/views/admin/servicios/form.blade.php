<div wire:ignore.self id="con-close-modal-servicio" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                @if(!$selected_id)
                    {{--<h4 class="modal-title">{{$title}}</h4>--}}
                    <h4 class="modal-title">SERVICIOS | REGISTRAR</h4>
                @else
                    <h4 class="modal-title">SERVICIOS | EDITAR</h4>
                @endif
                {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Nombre Servicio</label>
                            <input wire:model.lazy="nombre" type="text" class="form-control" id="field-3" placeholder="Nombre Servicio" maxlength="150" oninput="this.value = this.value.replace(/[^a-zA]/,'')">
                            @error('nombre')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-3" class="control-label">Precio</label>
                            <input wire:model.lazy="precio" type="number" class="form-control" id="field-3" placeholder="Precio" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/ pattern = "^[0-9]{4}$" >
                            @error('precio')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group no-margin">
                            <label for="field-7" class="control-label">Descripción</label>
                            <textarea wire:model.lazy="descripcion" class="form-control" id="field-7" placeholder="Descripción del servicio" maxlength="250" > </textarea>
                            @error('descripcion')
                            <span class="text-sm text-danger">{{$message}}</span>
                            @enderror
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button wire:click="closeModal()" type="button" class="btn btn-secondary waves-effect">CANCELAR</button>
                @if(!$selected_id)
                    <button wire:click.prevent="store()" type="button" class="btn btn-primary waves-effect waves-light">REGISTRAR</button>
                @else
                    <button wire:click.prevent="update()" type="button" class="btn btn-primary waves-effect waves-light">ACTUALIZAR</button>
                @endif
            </div>
        </div>
    </div>
</div>
