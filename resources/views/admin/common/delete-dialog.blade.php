<div id="info-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="text-center">
                    <i class="dripicons-information h1 text-info"></i>
                    <h4 class="mt-2">MENSAJE DEL SISTEMA!</h4>
                    {{--{{$selected_id}}--}}
                    @if($accion == 'desactivar')
                    <p class="">Estas seguro de eliminar el registro?</p>
                        <button wire:click="cancelarEliminado()" type="button" class="btn btn-success my-2">CANCELAR</button>
                        <button wire:click="eliminar()" type="button" class="btn btn-danger my-2" data-dismiss="modal">ELIMINAR</button>
                    @else
                    <p class="">Estas seguro de activar el registro?</p>
                        <button wire:click="cancelarEliminado()" type="button" class="btn btn-danger my-2">CANCELAR</button>
                        <button wire:click="eliminar()" type="button" class="btn btn-success my-2" data-dismiss="modal">ACTIVAR</button>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
