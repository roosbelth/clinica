<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                        <li class="breadcrumb-item active">Starter</li>
                    </ol>
                </div>
                <h4 class="page-title">Roles</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <label for="inputPassword2" class="sr-only">Search</label>
                <input type="search" class="form-control" id="inputPassword2" placeholder="Search...">
            </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 ">

                <!--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#info-alert-modal">Info
                    Alert</button>-->
                <button type="button" class="btn btn-success waves-effect waves-light mr-1 float-right" data-toggle="modal"
                    data-target="#con-close-modal"><i class="mdi mdi-plus-circle mr-1"></i> AGREGAR</button>

        </div><!-- end col-->

        <div class="responsive-table-plugin col-12">
            <div class="card-box">

                {{--<div class="table-rep-plugin">--}}
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>ROL</th>
                                    <th>DESCRIPCIÓN </th>
                                    <th>FECHA CREACIÓN</th>
                                    <th>FECHA ACTUALIZACIÓN</th>
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $rol)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <th><span class="co-name text-uppercase">{{$rol->rol}}</span></th>
                                    <td>{{$rol->descripcion}}</td>
                                    <td>{{$rol->created_at}}</td>
                                    <td>{{$rol->updated_at}}</td>
                                    <td>
                                        {{--<button type="button" class="btn btn-info" data-toggle="modal"
                                            data-target="#info-alert-modal{{ $loop->iteration }}">Info
                                            Alert</button>--}}
                                        <button wire:click="edit({{$rol->id}})" type="button"
                                            class="btn btn-blue btn-sm waves-effect waves-light"><i
                                                class="fas fa-pen"></i></button>
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="#info-alert-modal{{ $loop->iteration }}"><i
                                                class="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                                @include('admin.common.delete-dialog')
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{--
                </div>--}}
                <nav class="p-1">
                    <ul class="justify-content-start">
                        {{$roles->firstItem()}} a {{$roles->lastItem()}} de {{$roles->total()}}entradas
                    </ul>
                    <ul class="pagination justify-content-end">
                        {{$roles->links()}}
                    </ul>
                </nav>
            </div>
        </div>

    </div>

    @include('admin.roles.create')


</div>

<script>
    document.addEventListener('DOMContentLoaded', function (){
        window.livewire.on('rol-add', msg=>{
            $('#con-close-modal').modal('hide')
        });

        window.livewire.on('show-modal', msg=>{
            $('#con-close-modal').modal('show')
        });
    });
</script>
